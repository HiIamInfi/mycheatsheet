# Header

Overview; Explanation; Purpose; [link](docs/index.md)

|Command|Explanation(what does it do)|Example        |Usefull Additions (e.g. common Flags)|
|---|---|---|---|
| cmd   | My first command           | ´cmd directory1´| ´cmd --help´ shows all options
|next cmd| next explanation|next example|another addition|
