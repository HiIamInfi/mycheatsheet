# Basic Markdown commands

The following is a list of markdown commands and funcions.

## Basic commands

| Element         | Usage                                | Function                                  | Addition                                                                                       |
| --------------- | ------------------------------------ | ----------------------------------------- | ---------------------------------------------------------------------------------------------- |
| Headlines       | `# Headline`                         | Adds a big headline                       | For smaller headlines use more `#` e.g. `###`                                                  |
| Horizontal lign | `------` or `======`                 | Adds a horizontal lign to the page        |                                                                                                |
| Italics         | `*Asterisk*` or `_Underscore_`       | Display surrounded content cursive        |                                                                                                |
| Bold            | `**Asterisk**` or `__Underscore__`   | Display surrounded content bold           | Can be combined with Italics ba using both e.g. `**_both_**`                                   |
| Strike through  | `~~Stroke~~`                         | Display surrounded content stroke through |                                                                                                |
| Links           | `[Content](https://www.example.com)` | Add a link to Text on the page            |                                                                                                |
| Quotes          | `> This is a quote`                  | Adding a blockquote to the page           | For quotes to be more than one line just keep writing and markdown will insert the line breaks |

## Lists and Tables

### Tables

| Element            | Usage              | Function                                                                                                                                           |
| ------------------ | ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| Row                | `|Text|Text|Text|` | Prints a row in a Table. All rows in a table must have the same number of columns                                                                  |
| Headlines          | `|H1|H2|H3|`       | The first row of a Table is used to define headlines for the columns                                                                               |
| HeadlinesAlignment | `|---|:---:|---:|` | The second row of a table defines how the text in each column gets aligned. Alignment is decided by the place of the `:` on the sides of the `---` |

### Lists

| Element       | Usage                 | Comment                                  |
| ------------- | --------------------- | ---------------------------------------- |
| Sorted List   | `1. Text` / `4. Text` | It does not matter what numbers are used |
| Unsorted List | `* Text` / `- Text`   | Unsorted Lists can use `*`, `-` or `+`   |

## Adding external content

| Usage                                 | Output                                                  | Function                |
| ------------------------------------- | ------------------------------------------------------- | ----------------------- |
| `![](Image URL)`                      | ![](icons/MarkdownLint.png)                             | Add an image            |
| `![alt text](Image URL "Hover Text")` | ![alt text](icons/MarkdownLint.png "This is a Message") | Image with hover text   |
| `<img src="Image URL">`               | <img src="../docs/icons/MarkdownLint.png">              | Image using inline HTML |
