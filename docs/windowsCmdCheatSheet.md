# Windows Commands

This page is a collection of usefull Windows commands that help you get around faster in Windows

| Command                      | Explanation                                                       |
| ---------------------------- | ----------------------------------------------------------------- |
| `WINDOWS` + `R`              | Run a Windows Command                                             |
| `CTRL` + `SHIFT` + `ESC`     | Open the Task Manager                                             |
| `cmd`                        | Open a Command Prompt (can be used to to run every command below) |
| `powershell`                 | Open a Powershell Prompt                                          |
| `winver`                     | See "About Windwows"                                              |
| `ncpa.cpl`                   | See Network connections                                           |
| `appwiz.cpl`                 | Add/Remove Software                                               |
| `control`                    | Open Control Panel                                                |
| `compmgmt.msc`               | Open Computer Manangement                                         |
| `wf.msc`                     | Open Windows Defender Firewall Configuration                      |
| `inetcpl.cpl`                | Open Internet Options                                             |
| `gpedit.cpl`                 | Open Group Policys (Windows 10 Pro only)                          |
| `regedit`                    | Open Registry                                                     |
| `services.msc`               | Open Windows Services                                             |
| `mstsc`                      | Open Remote Desktop                                               |
| `systempropertiesadvanced`   | Open Advanced System Propertie                                    |
| `useraccountcontrolsettings` | Open User Account Control Settings                                |
