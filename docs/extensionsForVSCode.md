# Recommended VS Code Extensions

This is a list of recommended VS Code extensions including functional Extensions aswell as cosmetic extensions to enhace the look of VS Code

## General Extensions

|                                             | Name            | Function                                                                                                                                                                               | Link                                                                                       |
| ------------------------------------------- | --------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| ![GitLabWorkflow](icons/GitLabWorkflow.png) | GitLab Workflow | Shows informations about the current remote Gitlab Repository including Issues, Merge Requests and Pipeline Status                                                                     | [Link](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow)      |
| ![GitLens](icons/GitLens.png)               | GitLens         | Shows all kinds of usefull informations regarding Git Version Control for the current repository aswell as inline informations about the last commit for this file and the contributor | [Link](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)                |
| ![Markdownlint](icons/MarkdownLint.png)     | markdownlint    | Markdown linting and style checking for Visual Studio Code                                                                                                                             | [Link](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) |

## Language Specific Extensions

|                                 | Name             | Function                                                                                     | Link                                                                              |
| ------------------------------- | ---------------- | -------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| ![Python](icons/Python.png)     | Python           | Adds Language Support for Python to Visual Studio Code (Linting, Debugging, Intellisense)    | [Link](https://marketplace.visualstudio.com/items?itemName=ms-python.python)      |
| ![C/C++](icons/c&cpp.png)       | C/C++            | Adds Language Support for C and C++ to Visual Studio Code (Debugging, Intellisense)          | [Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)    |
| ![C#](icons/csharp.png)         | C#               | Adds Language Support for C# to Visual Studio Code (Debugging, Intellisense)                 | [Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)      |
| ![HTML/CSS](icons/html_css.png) | HTML CSS Support | Adds CSS Support for HTML Documents to Visual Studio Code (Linting, Debugging, Intellisense) | [Link](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css) |
| ![PowerShell](icons/pshell.png) | Powershell       | Provides rich PowerShell language support for Visual Studio Code                             | [Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode.PowerShell)  |

## Cosmetical Extensions and Themes

|                                          | Name                    | Function                                                                                       | Link                                                                                       |
| ---------------------------------------- | ----------------------- | ---------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| ![MaterialIcon](icons/MaterialIcons.png) | Material Icon Theme     | Adds a material Icon Theme to Visual Studio Code                                               | [Link](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)      |
| ![Material](icons/MaterialTheme.png)     | Material Theme          | Adds multiple material type themes to Visual Studio Code                                       | [Link](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme) |
| ![SynthWave](icons/SynthWave.png)        | Synth Wave '84 Theme    | Neon heavy theme for Visual Studio Code                                                        | [Link](https://marketplace.visualstudio.com/items?itemName=RobbOwen.synthwave-vscode)      |
| ![Rainbow](icons/Rainbow.png)            | Rainbow Brackets        | Highlights Brackets to make opening and the corresponding closing brackets more obvious        | [Link](https://marketplace.visualstudio.com/items?itemName=2gua.rainbow-brackets)          |
| ![Prettier](icons/Prettier.png)          | Prettier Code Formatter | Add automatic formatting for a selection of languages including CSS, JavaScript, Markdown etc. | [Link](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)         |
