# About

This project is built with Mkdocs. Mkdocs is a Python based tool that builds static Webpages from Markdown Documents.

Visit [mkdocs.org](http://mkdocs.org) for the Documentation.  
The Custom Theme used for this Project is called "Material". It can be found on [the Github Documentation Page](https://squidfunk.github.io/mkdocs-material/)

## Commands

- `mkdocs new [dir-name]` - Create a new project.
- `mkdocs serve` - Start the live-reloading docs server.
- `mkdocs build` - Build the documentation site.
- `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files
